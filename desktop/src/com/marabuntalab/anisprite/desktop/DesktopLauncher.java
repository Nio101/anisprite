package com.marabuntalab.anisprite.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.marabuntalab.anisprite.AniSprite;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DesktopLauncher  extends JFrame{


    private static final String PREFERRED_LOOK_AND_FEEL = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";

    private JFileChooser file;
    private JTextField filename;
    private JTextField rows;
    private JTextField cols;
    private JTextField totalFrames;
    private JTextField period;
    private JButton select;
    private JButton play;
    private JPanel panel1;
    private JPanel panel2;
    private JLabel credits;


    public DesktopLauncher(){
        file = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Images", "png", "jpg", "gif");
        file.setFileFilter(filter);

        filename = new JTextField();
        rows = new JTextField();
        cols = new JTextField();
        totalFrames = new JTextField();
        period = new JTextField();

        credits = new JLabel("AniSprite Tester by F. Matera", SwingConstants.CENTER);
        credits.setPreferredSize(new Dimension(200, 60));
        Font f = credits.getFont();
        credits.setFont(f.deriveFont(f.getStyle() | Font.BOLD));

        select = new JButton("Select");
        select.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                int result = file.showOpenDialog(new JFrame());
                if (result == JFileChooser.APPROVE_OPTION)
                    filename.setText(file.getSelectedFile().getAbsolutePath());
            }
        });

        play = new JButton("PLAY");
        play.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                try {
                    String [] params = new String[]{
                            file.getSelectedFile().getAbsolutePath(),
                            rows.getText(),
                            cols.getText(),
                            totalFrames.getText(),
                            period.getText()
                    };

                    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
                    config.width = 512;
                    config.height = 512;
                    new LwjglApplication(new AniSprite(params[0], params[1], params[2], params[3], params[4]), config);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        setLayout(new BorderLayout());

        panel1 = new JPanel(new GridLayout(0,3));

        panel1.add(new JLabel("Image:", SwingConstants.CENTER));
        panel1.add(filename);
        panel1.add(select);
        panel1.add(new JLabel("Atlas rows:", SwingConstants.CENTER));
        panel1.add(rows);
        panel1.add(new JLabel());
        panel1.add(new JLabel("Atlas columns:", SwingConstants.CENTER));
        panel1.add(cols);
        panel1.add(new JLabel());
        panel1.add(new JLabel("Total frames:", SwingConstants.CENTER));
        panel1.add(totalFrames);
        panel1.add(new JLabel());
        panel1.add(new JLabel("Period (seconds):", SwingConstants.CENTER));
        panel1.add(period);
        panel1.add(new JLabel());
        panel1.add(new JLabel());
        panel1.add(new JLabel());
        panel1.add(new JLabel());
        panel1.add(new JLabel());
        panel1.add(play);

        add(credits, BorderLayout.NORTH);
        add(panel1);
        pack();
    }



    public static void main(String args[]){
        try {
            String lnfClassname = PREFERRED_LOOK_AND_FEEL;
            if (lnfClassname == null)
                lnfClassname = UIManager.getCrossPlatformLookAndFeelClassName();
            UIManager.setLookAndFeel(lnfClassname);
        } catch (Exception e) {
            System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL
                    + " on this platform:" + e.getMessage());
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                DesktopLauncher frame = new DesktopLauncher();
                frame.setDefaultCloseOperation(DesktopLauncher.EXIT_ON_CLOSE);
                frame.setTitle("AniSprite");
                frame.getContentPane().setPreferredSize(frame.getSize());
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }
}
