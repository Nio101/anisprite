package com.marabuntalab.anisprite;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

public class AniSprite extends ApplicationAdapter {
	String imageFile;
    int rows, cols, totalFrames;
    float period;

    SpriteBatch batch;
	Texture img;
    Animation animation;
    float stateTime;


    public AniSprite(String imageFile, String rows, String cols, String totalFrames, String period){
        this.imageFile = imageFile;
        this.rows = Integer.parseInt(rows);
        this.cols = Integer.parseInt(cols);
        this.totalFrames = Integer.parseInt(totalFrames);
        this.period = Float.parseFloat(period);
    }


	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture(Gdx.files.absolute(imageFile));
        animation = createAnimation(img, rows, cols, totalFrames, period);
        stateTime = 0;
	}

	@Override
	public void render () {
        stateTime += Gdx.graphics.getDeltaTime();
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
        batch.draw(animation.getKeyFrame(stateTime, true), 0, 0);
		batch.end();
	}



    private static Animation createAnimation(Texture texture, int rows, int cols, int numFrames, float period){
        TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth()/cols, texture.getHeight()/rows);
        TextureRegion[] frames = new TextureRegion[numFrames];
        int index = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (index < numFrames)
                    frames[index++] = tmp[i][j];
            }
        }

        return new Animation(period, frames);
    }

}
