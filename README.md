# AniSprite

Have you ever needed to quickly test your animation without put it in your game?
I do, and my graphic designer too, so i wrote this simple utility, based on LibGDX, that performs the test for you.



##Setting up and run the project

### Direct Download
For the TL;DR people, this is the link to the compiled JAR:
[AniSprite.jar](https://bitbucket.org/Nio101/anisprite/downloads/AniSprite.jar)

### Get the source code
Open terminal window and launch this:
    $ git clone https://bitbucket.org/Nio101/anisprite.git


### Compile
Open terminal in the AniSprite directory, then:

    $ ./gradlew desktop:dir

When done, your brand new JAR executable is in *desktop/build/libs/*, you can rename it in "AniSprite.jar"


### Run
Open terminal in the jar directory, then:
    
    $ java -jar AniSprite.jar


## Usage

![Screen](https://bitbucket.org/Nio101/anisprite/downloads/AniSprite_screen.png)

* Select the image file, this must be an atlas of frames like this example in LibGDX wiki
![frames](https://raw.githubusercontent.com/wiki/libgdx/libgdx/images/sprite-animation1.png)

* Insert the number of rows in the atlas (in the example is 5);
* Insert the number of columns in the atlas (in the example is 6);
* Insert the total number of frames in the atlas; this is necessary because maybe not the whole image is filled of frames
 (in the example is 30);
* Insert the period between two frames in seconds; for example if you want 20 FPS the period must be 1/20 = 0.05;
* Press "Play" for run the animation.